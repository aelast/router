<?php

namespace Aelast\Router;

use Aelast\Request\Request as Request;
use Aelast\DataStorage\DataStorage as DataStorage;

class Router
{

    const SEPARATOR = '/';
    const URI_RELATIVE = 'relative';
    const URI_ABSOLUTE = 'active';

    protected $url;
    protected $config;
    protected $module;
    protected $controller;
    protected $action;
    protected $aliases = [];
    protected $default_module;
    protected $default_controller;
    protected $default_action;

    public function __construct(Array $config)
    {
        $this->config = new DataStorage($config);
        $this->default_module = $this->config->get('default_module', 'default');
        $this->default_controller = $this->config->get('default_controller', 'default');
        $this->default_action = $this->config->get('default_action', 'index');
        $this->aliases = $this->config->get('aliases', []);
        $this->init();
    }

    protected function init()
    {
        $url = trim(substr_replace(Request::getPath(), '', 0, strlen(Request::getBasePath())), self::SEPARATOR);
        $this->route_string = empty($url) ? self::SEPARATOR : $url;
        $segments = $this->extractSegments($this->route_string);
        $this->module = $segments[0];
        $this->controller = $segments[1];
        $this->action = $segments[2];
    }

    protected function extractSegments($path)
    {
        if (!is_array($path)) {
            $route_string = isset($this->aliases[$path]) ? $this->aliases[$path] : $path;
            $segments = explode(self::SEPARATOR, $route_string);
        } else {
            $segments = $path;
        }
        $segments[0] = !empty($segments[0]) ? strtolower($segments[0]) : strtolower($this->default_module);
        $segments[1] = !empty($segments[1]) ? strtolower($segments[1]) : strtolower($this->default_controller);
        $segments[2] = !empty($segments[2]) ? strtolower($segments[2]) : strtolower($this->default_action);
        return $segments;
    }

    protected function prepareRouteString($path)
    {
        return implode(self::SEPARATOR, extractSegments($path));
    }

    public function getModule()
    {
        return ucfirst($this->module);
    }

    public function getController()
    {
        return ucfirst($this->controller);
    }

    public function getAction()
    {
        return ucfirst($this->action);
    }

    /**
     * Возвращает базовый URL приложения
     * 
     * @return string url
     */
    public function getBaseUrl()
    {
        $url = ($this->config->get('uri_type', self::URI_RELATIVE) == self::URI_ABSOLUTE ?
                Request::getProtocol() . '://' . Request::getHost() : '')
            . Request::getBasePath()
            . self::SEPARATOR;
        return $url;
    }
    
    public function buildRoute($path = null)
    {
        $route_string = is_null($path) ? $this->route_string : $this->prepareRouteString($path);
        $aliases = array_flip($this->aliases);
        if (isset($aliases[$route_string])) {
            $route_string = $aliases[$route_string];
        }
        if ($route_string != self::SEPARATOR && $route_string != '') {
            return $route_string . self::SEPARATOR;
        }
        return '';
    }

    public function getRoute($separator = self::SEPARATOR, $lower = true)
    {
        $path = $this->getModule() . $separator .
            $this->getController() . $separator .
            $this->getAction();
        if ($lower) {
            return strtolower($path);
        }
        return $path;
    }

    /**
     * Возвращает query строку
     * 
     * @param array $params
     * @param string $separator
     * @return string
     */
    public function buildQuery($params = null, $separator = '&')
    {
        if (is_array($params)) {
            return http_build_query($params, '', $separator);
        }
        return '';
    }

    /**
     * Возвращает URL
     * 
     * @param string $path
     * @param array $params
     * @param string $separator
     * @return string
     */
    public function build($path = null, $params = null, $separator = '&')
    {
        $url = $this->getBaseUrl() . $this->buildRoute($path);
        if (is_array($params) && count($params) > 0) {
            $queryString = $this->buildQuery($params, $separator);
            $url .= (strpos($url, '?') === false ? '?' : $separator) . $queryString;
        }
        return $url;
    }

    /**
     * Возвращает URL текущей страницы.
     * Параметры в query string могут быть дополнены или заменены значениями из $params
     * 
     * @param array $params
     * @param string $separator
     * @return string
     */
    public function url($params = null, $separator = '&')
    {
        $queryParams = Request::getAllParams();
        if (is_array($params)) {
            foreach ($params as $key => $value) {
                if ((is_null($value) || strlen($value) == 0) && isset($queryParams[$key])) {
                    unset($queryParams[$key]);
                    continue;
                }
                $queryParams[$key] = $value;
            }
        }
        $url = $this->build(null, $queryParams, $separator);
        return $url;
    }

    /**
     * Выполняет редирект
     * 
     * @param type $path
     * @param type $arguments
     */
    public function redirect($path, $arguments = null)
    {
        $url = $this->build($path, $arguments);
        Header('Location: ' . $url);
        exit();
    }

}
